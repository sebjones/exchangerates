## Exchange Rates Application

This is a simple application designed to demonstrate adding and removing react components (in this case Exchange Rate boards), each of which will independently interact with an api
to update the exchange rate for the given currencies on the board every 5 seconds. 

The api itself is mocked so the values generated are entirely random (so some pretty wild exchange rate variations!).


## Improvements

There are still a number of key improvements to be made which I will continue to work through:

* Improve logic in form to only allow selection of combinations of currencies that have not already been added.
* Fix issue where no default value is selected for the dropdowns before a manual selection is made (blank values are passed to api).
* Add error handling and logic to mock api to prevent submission of blank / invalid currencies. 
* Add tests. 
* Improve styling and board design - currently a rush job!


