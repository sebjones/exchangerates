import delay from './delay';

const currencies = [
  'GBP',
  'EUR',
  'USD'
];

class ExchangeRateApi {
  static getCurrencies = () => {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        resolve(Object.assign([], currencies));
      }, delay);
    });
  };

  static getRates = (fromCurrency, toCurrency) => {
    console.log('getRates: ' + fromCurrency + '->' + toCurrency);
    return new Promise((resolve, reject) => {
      const rate = (Math.random() * (2.000 - 0.000)).toFixed(3);
      const inverse = (1 / rate).toFixed(3);
      console.log(rate);
      setTimeout(() => {
        resolve({
          from: fromCurrency,
          to: toCurrency,
          dateTime: Date.now,
          rate: rate,
          inverse: inverse
        })
      });
    });
  };
}

export default ExchangeRateApi;