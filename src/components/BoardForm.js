import React from 'react';
import { connect } from 'react-redux';
import { addBoard } from '../store/actions/boardActions';

class BoardForm extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      from: '',
      to: ''
    };

    this.onSubmit = this.onSubmit.bind(this);
    this.onChange = this.onChange.bind(this);
  }

  onChange(e) {
    this.setState({
      [e.target.name]: e.target.value
    });
  }

  onSubmit(e) {
    e.preventDefault();
    this.props.addBoard({
      from: this.state.from,
      to: this.state.to,
      datetime: null,
      rate: null
    });
  }

  render() {
    const { currency } = this.props;
    return (
      <div className="container">
        <div className="row">
          <div className="col-sm-12">
            <form onSubmit={this.onSubmit}>
              <div className="form-group">
                <label htmlFor="from">From:</label>
                <select className="form-control" value={this.state.from} name="from" id="from" onChange={this.onChange}>
                  {currency && currency.list && currency.list.map((item, index) =>
                    <option key={index}>{item}</option>
                  )}
                </select>
              </div>
              <div className="form-group">
                <label htmlFor="to">To:</label>
                <select className="form-control" value={this.state.to} name="to" id="to" onChange={this.onChange}>
                  {currency && currency.list && currency.list.map((item, index) =>
                    <option key={index}>{item}</option>
                  )}
                </select>
              </div>
              <input type="submit" value="Add Exchange Rates" className="btn btn-info btn-block mt-4" />
              <br />
            </form>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  currency: state.currency
});

export default connect(mapStateToProps, { addBoard })(BoardForm);