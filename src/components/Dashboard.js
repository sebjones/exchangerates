import React from 'react';
import BoardList from './BoardList';
import BoardForm from './BoardForm';

class Dashboard extends React.Component {
  render() {
    return (
      <div className="dashboard">
        <div className="container">
          <div className="row">
            <div className="col-md-8 m-auto">
              <h1 className="display-4 text-center">Exchange Rates</h1>
              <BoardForm />
              <BoardList />
            </div>
          </div>
        </div>

      </div>
    );
  }
}

export default Dashboard;