import React from 'react';
import { connect } from 'react-redux';
import { getRates } from '../store/actions/boardActions';
import Board from './Board';

class BoardList extends React.Component {
  render() {
    const { boards } = this.props.dashboard;
    return (
      <div>
        {boards.map((board, index) =>
          <Board key={index} board={board} />
        )}
      </div>
    );
  }
}

const mapStateToProps = state => ({
  dashboard: state.dashboard,
  currency: state.currency
});

export default connect(mapStateToProps, { getRates })(BoardList);