import React from 'react';
import { connect } from 'react-redux';
import { getRates, deleteBoard } from '../store/actions/boardActions';
import '../styles/styles.css';

class Board extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      dashboard: null
    }
  }

  deleteBoard = () => {
    this.props.deleteBoard(this.props.board);
  };

  componentDidMount() {
    this.interval = setInterval(() => {
      this.props.getRates({
        from: this.props.board.from,
        to: this.props.board.to
      });
    }, 5000);
  }

  componentWillReceiveProps(nextProps) {
    this.setState(() => {
      return {
        dashboard: nextProps.dashboard
      }
    });
  }

  componentWillUnmount() {
    clearInterval(this.interval);
  }

  render() {
    let { board } = this.props;

    return (
      <div className="custom-jumbotron">
        <div className="container">
          <div className="row">
            <div className="col-sm-6">
              <span>
                <strong>{board.from}</strong> to <strong>{board.to}</strong>
              </span>
            </div>
            <div className="col-sm-5">
              <span>{board.rate} / {(1 / board.rate).toFixed(3)}</span>
            </div>
            <div className="col-sm-1">
              <i className="fas fa-times pointer" onClick={() => this.deleteBoard()} title="Delete Board"></i>
            </div>
          </div>
        </div>
      </ div >
    );
  }
}

export default connect(null, { getRates, deleteBoard })(Board);