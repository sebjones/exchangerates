import { GET_CURRENCIES } from './actionTypes';
import ExchangeRateApi from '../../api/mockExchangeRateApi';

export const getCurrencies = () => dispatch => {
  ExchangeRateApi.getCurrencies()
    .then(res => {
      dispatch({
        type: GET_CURRENCIES,
        payload: res
      });
    }
    );
};