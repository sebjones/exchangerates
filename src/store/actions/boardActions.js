import { ADD_BOARD, UPDATE_BOARD, DELETE_BOARD } from './actionTypes';
import ExchangeRateApi from '../../api/mockExchangeRateApi';

export const addBoard = (boardData) => dispatch => {
  dispatch({
    type: ADD_BOARD,
    payload: boardData
  });
};

export const deleteBoard = (board) => dispatch => {
  dispatch({
    type: DELETE_BOARD,
    payload: board
  });
};

export const getRates = (currencies) => dispatch => {
  ExchangeRateApi.getRates(currencies.from, currencies.to)
    .then(res => {
      dispatch({
        type: UPDATE_BOARD,
        payload: res
      });
    })
};