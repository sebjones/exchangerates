import { ADD_BOARD, DELETE_BOARD, UPDATE_BOARD } from '../actions/actionTypes';

const initialState = {
  boards: []
};

export default (state = initialState, action) => {
  switch (action.type) {
    case ADD_BOARD:
      return {
        ...state,
        boards: [...state.boards, action.payload]
      };
    case UPDATE_BOARD:
      const updatedBoards = state.boards.map((item, index) => {
        if (item.from + item.to !== action.payload.from + action.payload.to) {
          return item;
        }

        return {
          ...item,
          ...action.payload
        };
      });

      return {
        ...state,
        boards: updatedBoards
      };
    case DELETE_BOARD:
      return {
        ...state,
        boards: state.boards.filter(item => item !== action.payload)
      };
    default:
      return state;
  }
};