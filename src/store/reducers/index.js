import { combineReducers } from 'redux';
import currencyReducer from './currencyReducer';
import boardReducer from './boardReducer';

export default combineReducers({
  currency: currencyReducer,
  dashboard: boardReducer
});