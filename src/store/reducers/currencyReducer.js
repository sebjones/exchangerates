import { GET_CURRENCIES } from '../actions/actionTypes';

const initialState = {
  list: null
};

export default (state = initialState, action) => {
  switch (action.type) {
    case GET_CURRENCIES:
      return {
        ...state,
        list: action.payload
      };
    default:
      return state;
  }
};