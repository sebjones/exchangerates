import React, { Component } from 'react';
import { Provider } from 'react-redux';
import Dashboard from './components/Dashboard';
import './App.css';

import { getCurrencies } from './store/actions/currencyActions';
import { addBoard } from './store/actions/boardActions';
import store from './store/store';

store.dispatch(getCurrencies())
store.dispatch(addBoard({
  from: 'GBP',
  to: 'EUR',
  datetime: null,
  rate: null
}));
store.dispatch(addBoard({
  from: 'USD',
  to: 'GBP',
  datetime: null,
  rate: null
}));

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <Dashboard />
      </Provider>
    );
  }
}

export default App;
